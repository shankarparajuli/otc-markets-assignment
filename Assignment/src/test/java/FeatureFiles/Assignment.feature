Feature: Assignment

  Scenario Outline: OTC Markets

    Given the user login OTCMarkets
    And the user searches "<Text>" in quote textbox
    When the user navigates to Quote tab
    And the user captures Open and Market cap
    Then the user navigate to Security Details tab
    And print the Market Cap details

    Examples:
    |Text|
    |OTCM|
    |RHHBY|
    |ADBCF|
