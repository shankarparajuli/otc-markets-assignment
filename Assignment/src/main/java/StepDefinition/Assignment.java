package StepDefinition;

import Util.ConfigReader;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Assignment {

    private WebDriver driver;
    private ConfigReader configReader;
    Properties prop;

    @Before(order = 0)
    public void getProperty() {
        configReader = new ConfigReader();
        prop = configReader.init_prop();
    }

    @Before(order = 1)
    public void launchBrowser() {
        String browser = prop.getProperty("browser");

        if (browser.equals("chrome")) {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
        } else if (browser.equals("firefox")) {
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();
        } else if (browser.equals("safari")) {
            driver = new SafariDriver();
        } else {
            System.out.println("Please pass the correct browser value: " + browser);
        }
    }

    @After(order = 0)
    public void quitBrowser() {
        driver.quit();
    }

    @After(order = 1)
    public void tearDown(Scenario scenario) {
        if (scenario.isFailed()) {
            // take screenshot:
            String screenshotName = scenario.getName().replaceAll(" ", "_");
            byte[] sourcePath = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            scenario.attach(sourcePath, "image/png", screenshotName);

        }
    }

    /** To Wait Until Element to be Visible */
    public void explicitWaitvisibilityOfElementLocated(By element, long timeInSeconds) {
        WebDriverWait elementToBeVisible = new WebDriverWait(driver, timeInSeconds);
        elementToBeVisible.until(ExpectedConditions.visibilityOfElementLocated(element));
    }



    @Given("the user login OTCMarkets")
    public void the_user_login_otc_markets() {
        driver.get("https://www.otcmarkets.com/");
        driver.manage().window().maximize();

    }



    @Given("the user searches {string} in quote textbox")
    public void the_user_searches_in_quote_textbox(String string) throws InterruptedException {
        driver.findElement(By.xpath("(//input[@placeholder=\"Quote\"])[1]")).sendKeys(string);
        Thread.sleep(4000);
        prop.setProperty("MarketQuote",string);
        driver.findElement(By.xpath("(//input[@placeholder=\"Quote\"])[1]")).sendKeys(Keys.ENTER);
        explicitWaitvisibilityOfElementLocated(By.xpath("//a[text()='Quote']"),15);

    }
    @When("the user navigates to Quote tab")
    public void the_user_navigates_to_quote_tab() {
        explicitWaitvisibilityOfElementLocated(By.xpath("//a[text()='Quote']"),15);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[text()='Quote']")));


    }
    @When("the user captures Open and Market cap")
    public void the_user_captures_open_and_market_cap() {
        // Write code here that turns the phrase above into concrete actions
        try {
            String open_Value = driver.findElement(By.xpath("//label[text()='Open']/following-sibling::p")).getText();
            String MarketCap_Value = driver.findElement(By.xpath("//label[text()='Market Cap']/ancestor::span/parent::div/p")).getText();
            prop.setProperty("open_Value",open_Value);
            prop.setProperty("MarketCap_Value",MarketCap_Value);

        }catch (Exception e){
            e.printStackTrace();
        }


        Assert.assertTrue(driver.findElement(By.xpath("//img[@src=\"/logos/tier/QX.png\"]")).isDisplayed());
        Assert.assertTrue(driver.findElement(By.xpath("(//h1)[1]")).isDisplayed());

    }
    @Then("the user navigate to Security Details tab")
    public void the_user_navigate_to_security_details_tab() {
        explicitWaitvisibilityOfElementLocated(By.xpath("//a[text()='Security Details']"),15);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[text()='Security Details']")));


    }
    @Then("print the Market Cap details")
    public void print_the_market_cap_details() {
        // Write code here that turns the phrase above into concrete actions
        System.out.println(driver.findElement(By.xpath("//label[text()='Market Cap']/ancestor::b/parent::div/following-sibling::div[1]")).getText());
                System.out.println(driver.findElement(By.xpath("//label[text()='Market Cap']/ancestor::b/parent::div/following-sibling::div[2]")).getText());

       Assert.assertTrue(driver.findElement(By.xpath("//label[text()='Market Cap']/ancestor::b/parent::div/following-sibling::div[1]")).getText().equals(prop.getProperty("MarketCap_Value")));
    }



}
